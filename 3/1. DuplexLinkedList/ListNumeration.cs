﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuplexLinkedList
{
    class ListNumeration<T> : IEnumerator<T>
    {
        D<T> DList;

        public ListNumeration(D<T> list)
        {
            DList = list; 
        }

        public SomeListItem<T> currentElem;        

        public T Current => currentElem.data;

        object IEnumerator.Current => throw new NotImplementedException();

        public void Dispose()
        {
            DList = null;
        }

        public bool MoveNext()
        {
            if(currentElem == null)
            {
                currentElem = DList.head;
                return true;

            }

            if (currentElem.next != null)
            {                
                currentElem = currentElem.next;
                return true;
            }
            else return false;
        }

        public void Reset()
        {
            currentElem = DList.head;
            
        }
    }
}
